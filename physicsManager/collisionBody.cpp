#include "pch.h"
#include "collisionBody.h"

CollisionBody::CollisionBody() :Body()
{
}

CollisionBody::~CollisionBody()
{
	
}

void CollisionBody::operator+(CollisionBody* hittedBody)
{
	Eigen::Vector3d thisForce = forceApplier.getSumOfForces();
	Eigen::Vector3d otherForce = hittedBody->getForceApplier()->getSumOfForces();

	Eigen::Vector3d newForce = thisForce + otherForce;

	unityCallbacks.debugLog("Hitting hitted body");
}
