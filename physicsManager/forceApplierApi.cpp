#include "pch.h"
#include "forceApplierApi.h"


UNITY_EXPORT void addForceToSideLeftContacts(PhysicsBody* physicsBody, double left, float leftTime)
{
	physicsBody->getForceApplier()->addForce<Left>(left, leftTime);
	physicsBody->getForceApplier()->setActivePoints(true);
}

UNITY_EXPORT void addForceToSideRightContacts(PhysicsBody* physicsBody, double right, float rightTime)
{
	physicsBody->getForceApplier()->addForce<Right>(right, rightTime);
	physicsBody->getForceApplier()->setActivePoints(true);
}

UNITY_EXPORT void addForceToSideBottomContacts(PhysicsBody* physicsBody, double bottom, float bottomTime)
{
	physicsBody->getForceApplier()->addForce<Bottom>(bottom, bottomTime);
	physicsBody->getForceApplier()->setActivePoints(true);
}

UNITY_EXPORT void addForceToSideUpContacts(PhysicsBody* physicsBody, double up, float upTime)
{
	physicsBody->getForceApplier()->addForce<Up>(up, upTime);
	physicsBody->getForceApplier()->setActivePoints(true);
}

UNITY_EXPORT void addForceToSideFrontContacts(PhysicsBody* physicsBody, double back, float backTime)
{
	physicsBody->getForceApplier()->addForce<Back>(back, backTime);
	physicsBody->getForceApplier()->setActivePoints(true);
}

UNITY_EXPORT void addForceToSideBackContacts(PhysicsBody* physicsBody, double front, float frontTime)
{
	physicsBody->getForceApplier()->addForce<Front>(front, frontTime);
	physicsBody->getForceApplier()->setActivePoints(true);
}



UNITY_EXPORT void applyForceToRigidBody(PhysicsBody* physicsBody)
{
	ForceApplier* forceApplier = physicsBody->getForceApplier();

	if(forceApplier->areThereActivePoints())
	{
		Eigen::Vector3d forces = forceApplier->getSumOfForces();

		physicsBody->getUnityCallbacks()->debugLog("test");

		physicsBody->getUnityCallbacks()->applyForceToRigidBody(forces.x(), forces.y(), forces.z());


		forceApplier->fixedUpdateApplyForceTime();
	}
}
