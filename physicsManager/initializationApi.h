#pragma once
#include "unityMeta.h"
#include "contactPoints.h"
#include "sideContacts.h"
#include "physicsBody.h"
#include "ressistanceBody.h"
#include "activeBody.h"
#include "unityCallbacksDefinition.h"



extern "C"
{
	UNITY_EXPORT void initialzieCallbackUnityDebugLog(PhysicsBody* physicsBody, UNITY_DEBUG unityDebug);

	UNITY_EXPORT PhysicsBody* initializePhysicsBody(int typeOfContacts,int typeOfBody);

	UNITY_EXPORT void deletePhysicsBody(PhysicsBody* physicsBody);

	UNITY_EXPORT void initializeCallbackApplyForceToRigidBody(PhysicsBody* physicsBody,APPLY_FORCE_TO_RIGIDBODY applyForceToRigidBody);

}


Contact* createContactPointsObject(int type);
PhysicsBody* createPhysicsBodyObject(int typeOfBody,Contact* contact);


