#pragma once
#include "unityCallbacksDefinition.h"

class UnityCallbacks 
{
public:
	UnityCallbacks() {};
	~UnityCallbacks() {};

	UNITY_DEBUG debugLog;
	APPLY_FORCE_TO_RIGIDBODY applyForceToRigidBody{};

protected:
private:

};
