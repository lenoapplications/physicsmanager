#pragma once
#include "body.h"

class CollisionBody : public Body 
{
public:
	CollisionBody();
	virtual ~CollisionBody();

	void operator +(CollisionBody* hittedBody);

private:

};

