#pragma once
#include <map>
#include <eigen-3.3.7/Eigen/Dense>
#include "contact.h"

class Side : public Contact::ContactPoint{
public:
	Side();
	virtual ~Side();
	virtual void applyForceToContactPoint(double force);

	void print();

private:

protected:
};


class Left : public Side {
public:
	Left() {};
	~Left() {};
	void applyForceToContactPoint(double force) override;
private:
protected:
};

class Right : public Side {
public:
	Right() {};
	~Right() {};
	void applyForceToContactPoint(double force) override;
private:
protected:
};

class Bottom : public Side {
public:
	Bottom() {};
	~Bottom() {};
	void applyForceToContactPoint(double force) override;
private:
protected:
};


class Up : public Side {
public:
	Up() {};
	~Up() {};
	void applyForceToContactPoint(double force) override;
private:
protected:
};


class Front : public Side {
public:
	Front() {};
	~Front() {};
	void applyForceToContactPoint(double force) override;
private:
protected:
};

class Back : public Side {
public:
	Back() {};
	~Back() {};
	void applyForceToContactPoint(double force) override;
private:
protected:
};