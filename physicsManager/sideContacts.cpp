#include "pch.h"
#include "sideContacts.h"

SideContacts::SideContacts():LEFT(std::string(typeid(Left).name())),
							 RIGHT(std::string(typeid(Right).name())),
							 BOTTOM(std::string(typeid(Bottom).name())),
							 UP(std::string(typeid(Up).name())),
							 FRONT(std::string(typeid(Front).name())),
							 BACK(std::string(typeid(Back).name())),
							 Contact()
{
	initializeSides();

}

SideContacts::~SideContacts()
{
	std::cout << "deleting Side contacts" << std::endl;
}

Eigen::Vector3d SideContacts::getSumOfForces()
{
	return Eigen::Vector3d (getSumX(), getSumY(), getSumZ());
}

void SideContacts::initializeSides()
{

	contactPoints.insert({ LEFT, std::unique_ptr<Side>(new Left() ) });
	contactPoints.insert({ RIGHT, std::unique_ptr<Side>(new Right() ) });

	contactPoints.insert({ BOTTOM , std::unique_ptr<Side>(new Bottom()) });
	contactPoints.insert({ UP , std::unique_ptr<Side>(new Up()) });

	contactPoints.insert({ FRONT , std::unique_ptr<Side>(new Front()) });
	contactPoints.insert({ BACK, std::unique_ptr<Side>(new Back()) });
}

double SideContacts::getSumX()
{
	double xSum = 0;
	xSum += (contactPoints[LEFT]->isActive()) ? contactPoints[LEFT]->getForce() : 0;
	xSum += (contactPoints[RIGHT]->isActive()) ? contactPoints[RIGHT]->getForce() : 0;
	return xSum;
}

double SideContacts::getSumY()
{
	double ySum = 0;
	ySum += (contactPoints[BOTTOM]->isActive()) ? contactPoints[BOTTOM]->getForce() : 0;
	ySum += (contactPoints[UP]->isActive()) ? contactPoints[UP]->getForce() : 0;
	return ySum;
}

double SideContacts::getSumZ()
{
	double zSum = 0;
	zSum += (contactPoints[BACK]->isActive()) ? contactPoints[BACK]->getForce() : 0;
	zSum += (contactPoints[FRONT]->isActive()) ? contactPoints[FRONT]->getForce() : 0;
	return zSum;
}



