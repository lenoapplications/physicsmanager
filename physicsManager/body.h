#pragma once
#include "contactPoints.h"
#include "sideContacts.h"
#include "unityCallbacks.h"
#include "forceApplier.h"

class Body
{

public:
	Body();
	virtual ~Body();

	ForceApplier* getForceApplier();
	UnityCallbacks* getUnityCallbacks();

private:
protected:
	ForceApplier forceApplier;
	UnityCallbacks unityCallbacks;
};

