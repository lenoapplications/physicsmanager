#include "pch.h"
#include "initializationApi.h"

//UNITY_EXPORT void testMethod(TEST test,char* ovo)
//{
//	test("OVO JE C++ TEXT");
//	MessageBox(NULL, L"OVDJE", (LPCWSTR)ovo, 0);
//}
//
//UNITY_EXPORT CCALLBACK ccallback()
//{
//	return [](char* ovo,char* buffer)  
//	{
//
//		int index = 0;
//
//		const char* toBuffer = "1234";
//
//		int size = strlen(toBuffer);
//		
//		auto s = std::to_wstring(size);
//
//		while(ovo[index] != '\0')
//		{
//			if (ovo[index] == 'E') {
//				MessageBox(NULL, L"OVDJE", s.c_str(), 0);
//			}
//			++index;
//		}
//		
//		strcpy_s(buffer,30,toBuffer);
//	
//  
//
//
//
//
//
//	};
//}



UNITY_EXPORT void initialzieCallbackUnityDebugLog(PhysicsBody* physicsBody, UNITY_DEBUG unityDebug)
{
	physicsBody->getUnityCallbacks()->debugLog = unityDebug;
}

UNITY_EXPORT PhysicsBody* initializePhysicsBody(int typeOfContacts,int typeOfBody)
{
	
	PhysicsBody* body = createPhysicsBodyObject(typeOfBody, createContactPointsObject(typeOfContacts));

	return body;
	
}

UNITY_EXPORT void deletePhysicsBody(PhysicsBody* physicsBody)
{
	delete physicsBody;
}

UNITY_EXPORT void initializeCallbackApplyForceToRigidBody(PhysicsBody* physicsBody,APPLY_FORCE_TO_RIGIDBODY applyForceToRigidBody)
{
	physicsBody->getUnityCallbacks()->applyForceToRigidBody = applyForceToRigidBody;
}





Contact* createContactPointsObject(int type)
{
	switch (type)
	{
		case 0:
			return new SideContacts();

		default:
			return new SideContacts();
	}
}
PhysicsBody* createPhysicsBodyObject(int type,Contact* contact)
{
	switch(type)
	{
		case 0:
			return new RessistanceBody(contact);
		case 1:
			return new ActiveBody(contact);
		default:
			return new RessistanceBody(contact);
	}
}
