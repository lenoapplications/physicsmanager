#pragma once
#include <queue>
#include <thread>


class ThreadSystem
{
public:
	ThreadSystem();
	~ThreadSystem();


	template<typename FUNCTION>
	void initThread(FUNCTION func);



protected:
private:

	std::queue<std::thread> threadsInQueue;
};

template<typename FUNCTION>
inline void ThreadSystem::initThread(FUNCTION func)
{
	std::thread a(func);

	threadsInQueue.push(a);
}
