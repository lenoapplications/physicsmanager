#pragma once
#include "contact.h"



class ContactPoints
{
public:
	ContactPoints();
	~ContactPoints();
	
	void setContact(Contact* contactObject);


	template<typename TYPE_OF_CONTACT>
	TYPE_OF_CONTACT* getContact();

	template<>
	Contact* getContact<Contact>();


private:
	Contact* contact = nullptr;

protected:
};


template<>
Contact* ContactPoints::getContact()
{
	return contact;
}

template<typename TYPE_OF_CONTACT>
inline TYPE_OF_CONTACT* ContactPoints::getContact()
{
	if(dynamic_cast<TYPE_OF_CONTACT*>(contact) != nullptr)
	{
		return dynamic_cast<TYPE_OF_CONTACT*>(contact);
	}else
	{
		return nullptr;
	}
}
