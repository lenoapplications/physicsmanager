#include "pch.h"
#include "contactPoints.h"

ContactPoints::ContactPoints()
{
}

ContactPoints::~ContactPoints()
{
	std::cout << "deleting contact points "<<contact << std::endl;
	if (contact != nullptr) {
		delete contact;
	}
}

void ContactPoints::setContact(Contact* contactObject)
{
	this->contact = contactObject;
}
