#include "pch.h"
#include "sides.h"

Side::Side()
{
	forceActingOnPoint = 0;
}

Side::~Side()
{
}


void Side::print()
{
	std::cout << forceActingOnPoint << std::endl;
}

void Side::applyForceToContactPoint(double force)
{
	std::cout << "General side " << std::endl;
}


void Left::applyForceToContactPoint(double force)
{
	this->forceActingOnPoint = force;
}

void Right::applyForceToContactPoint(double force)
{
	this->forceActingOnPoint = -force;
}

void Bottom::applyForceToContactPoint(double force)
{
	this->forceActingOnPoint = force;
}

void Up::applyForceToContactPoint(double force)
{
	this->forceActingOnPoint = -force;
}

void Back::applyForceToContactPoint(double force)
{
	this->forceActingOnPoint = force;
}

void Front::applyForceToContactPoint(double force)
{
	this->forceActingOnPoint = -force;
}


