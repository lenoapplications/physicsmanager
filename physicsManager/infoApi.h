#pragma once
#include "unityMeta.h"
#include "physicsBody.h"

extern "C"
{
	UNITY_EXPORT void giveContactForceVector(PhysicsBody* physicsBody,double vectorArray[],int sizeOfArray);
}
