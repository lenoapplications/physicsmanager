#pragma once
#include "forceTime.h"

class ForceApplier
{
public:
	ForceApplier();
	~ForceApplier();


	void initializeTypeOfContactPoints(Contact* contact);

	template<typename ContactPoint>
	void addForce(double force,float duration);

	Eigen::Vector3d getSumOfForces();

	void fixedUpdateApplyForceTime();

	void setActivePoints(bool status);
	bool areThereActivePoints();

	template<typename T>
	float getTimeTest() {
		Contact* contact = contactPoints.getContact<Contact>();
		 
		return contact->getContactPoint<T>()->getForceTimeDuration();
	};

protected:

private:
	ContactPoints contactPoints;
	ForceTime forceTime;

	bool activePoints;

};


template<typename ContactPoint>
inline void ForceApplier::addForce(double force,float duration)
{
	Contact* contact = contactPoints.getContact<Contact>();
	std::shared_ptr<Contact::ContactPoint> contactPoint = contactPoints.getContact<Contact>()->getContactPoint<ContactPoint>();

	contactPoint->applyForceToContactPoint(force);
	contactPoint->setApplyForceDurationTime(duration);
}


