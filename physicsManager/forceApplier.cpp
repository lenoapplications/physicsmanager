#include "pch.h"
#include "forceApplier.h"

ForceApplier::ForceApplier()
{
	this->forceTime = ForceTime{};
	this->contactPoints = ContactPoints{};
	this->activePoints = false;
}

ForceApplier::~ForceApplier()
{
}

void ForceApplier::initializeTypeOfContactPoints(Contact* contact)
{
	this->contactPoints.setContact(contact);
}



Eigen::Vector3d ForceApplier::getSumOfForces()
{
	return contactPoints.getContact<Contact>()->getSumOfForces();
}

void ForceApplier::fixedUpdateApplyForceTime()
{
	activePoints = contactPoints.getContact<Contact>()->fixedUpdateOfApplyForce();
}

void ForceApplier::setActivePoints(bool status)
{
	this->activePoints = status;
}

bool ForceApplier::areThereActivePoints()
{
	return activePoints;
}

