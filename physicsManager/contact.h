#pragma once
#include <map>
#include <eigen-3.3.7/Eigen/Dense>
#include "unityConstants.h"]




class Contact
{

public:

	class ContactPoint
	{
	public:
		ContactPoint() {};
		virtual ~ContactPoint() {};

		virtual void applyForceToContactPoint(double force) {};
		
		void setApplyForceDurationTime(float time) { this->applyForceDurationTime = time;}
		void fixedUpdateApplyForceDurationTime() { this->applyForceDurationTime -= UnityConstants::FIXED_UPDATE_DELTA_TIME; }

		double getForce() { return this->forceActingOnPoint; }
		float getForceTimeDuration() { return this->applyForceDurationTime; }
		bool isActive() { return this->applyForceDurationTime > 0; }
		void reset() { this->forceActingOnPoint = 0; }

	

	private:
	protected:
		double forceActingOnPoint;
		float applyForceDurationTime;
	};

	
	Contact();
	virtual ~Contact();
	virtual Eigen::Vector3d getSumOfForces();


	template<class CONTACT>
	std::shared_ptr<ContactPoint> getContactPoint();

	bool fixedUpdateOfApplyForce();


private:

protected:
	std::map<std::string, std::shared_ptr<ContactPoint>> contactPoints;
};

template<class CONTACT>
inline std::shared_ptr<Contact::ContactPoint> Contact::getContactPoint()
{
	std::map<std::string, std::shared_ptr<ContactPoint>>::iterator iterator = contactPoints.find(typeid(CONTACT).name());

	if (iterator != contactPoints.end())
	{
		std::shared_ptr<ContactPoint> desiredContactPoint = iterator->second;
		return desiredContactPoint;
	}
	return NULL;
}
