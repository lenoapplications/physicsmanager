#pragma once
#include "unityConstants.h"
#include "contactPoints.h"

class ForceTime
{
public:
	ForceTime();
	~ForceTime();


	void initApplyForceTime(float x, float y, float z);

	bool isTimeToApplyForce();
	
	void timeUpdateFixedUpdate();
protected:
private:
	float xApplyForceDuration;
	float yApplyForceDuration;
	float zApplyForceDuration;

};

