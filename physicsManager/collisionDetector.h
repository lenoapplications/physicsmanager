#pragma once
#include "unityMeta.h"
#include "physicsBody.h"

extern "C"
{
	UNITY_EXPORT void onBodyHit(PhysicsBody* thisBody, PhysicsBody* hittedBody);
}