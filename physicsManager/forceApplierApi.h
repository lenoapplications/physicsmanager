#include "unityMeta.h"
#include "forceApplierDefinition.h"
#include "physicsBody.h"

//SIDE CONTACT TYPE - API FUNCTIONS

extern "C"
{
	UNITY_EXPORT void addForceToSideLeftContacts(PhysicsBody* physicsBody, double left,float leftTime);
	UNITY_EXPORT void addForceToSideRightContacts(PhysicsBody* physicsBody, double right,float rightTime);
	UNITY_EXPORT void addForceToSideBottomContacts(PhysicsBody* physicsBody, double bottom,float bottomTime);
	UNITY_EXPORT void addForceToSideUpContacts(PhysicsBody* physicsBody, double up,float upTime);
	UNITY_EXPORT void addForceToSideFrontContacts(PhysicsBody* physicsBody, double back,float backTime);
	UNITY_EXPORT void addForceToSideBackContacts(PhysicsBody* physicsBody, double front,float frontTime);


	UNITY_EXPORT void applyForceToRigidBody(PhysicsBody* physicsBody);
}