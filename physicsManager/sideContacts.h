#pragma once
#include "contact.h"
#include "sides.h"



class SideContacts : public Contact
{
public:
	SideContacts();
	~SideContacts();


	Eigen::Vector3d getSumOfForces() override;


private:
	const std::string LEFT;
	const std::string RIGHT;
	const std::string BOTTOM;
	const std::string UP;
	const std::string FRONT;
	const std::string BACK;


	void initializeSides();
	double getSumX();
	double getSumY();
	double getSumZ();

protected:

};


