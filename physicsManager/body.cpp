#include "pch.h"
#include "body.h"

Body::Body()
{
	this->forceApplier = ForceApplier{};
	this->unityCallbacks = UnityCallbacks{};
}

Body::~Body()
{

}

ForceApplier* Body::getForceApplier()
{
	return &this->forceApplier;
}

UnityCallbacks* Body::getUnityCallbacks()
{
	return &this->unityCallbacks;
}
