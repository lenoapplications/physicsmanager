#include "pch.h"
#include "forceTime.h"

ForceTime::ForceTime()
{
	this->xApplyForceDuration = 0;
	this->yApplyForceDuration = 0;
	this->zApplyForceDuration = 0;
}

ForceTime::~ForceTime()
{
}

void ForceTime::initApplyForceTime(float x, float y, float z)
{
	this->xApplyForceDuration = x;
	this->yApplyForceDuration = y;
	this->zApplyForceDuration = z;
}

bool ForceTime::isTimeToApplyForce()
{
	return xApplyForceDuration > 0 || yApplyForceDuration > 0 || zApplyForceDuration > 0;
	
}



void ForceTime::timeUpdateFixedUpdate()
{
	this->xApplyForceDuration -= UnityConstants::FIXED_UPDATE_DELTA_TIME;
	this->yApplyForceDuration -= UnityConstants::FIXED_UPDATE_DELTA_TIME;
	this->zApplyForceDuration -= UnityConstants::FIXED_UPDATE_DELTA_TIME;
}
