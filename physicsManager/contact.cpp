#include "pch.h"
#include "contact.h"




Contact::Contact()
{
	std::cout << "Contact Initialized" << std::endl;
}

Contact::~Contact()
{
	std::cout << "Deleting contact general" << std::endl;
}

Eigen::Vector3d Contact::getSumOfForces()
{
	return Eigen::Vector3d();
}

bool Contact::fixedUpdateOfApplyForce()
{
	bool areThereActivePoints = false;
	std::map<std::string, std::shared_ptr<ContactPoint>>::iterator it;

	for( it = contactPoints.begin(); it != contactPoints.end(); it++)
	{
		it->second->fixedUpdateApplyForceDurationTime();

		if (it->second->isActive()) areThereActivePoints = true;
		else it->second->reset();
	}
	return areThereActivePoints;
}

